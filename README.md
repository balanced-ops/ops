# Infrastructure Operations

Some useful utilities used internall by Balanced to manage infrastructure

## NOTES

The `aws_credentials` parameter that's used in some tasks expects a JSON string
representing a dictionary with these fields:

     AWS_DEFAULT_REGION
     AWS_ACCESS_KEY_ID
     AWS_SECRET_ACCESS_KEY

## Free GUI!

You can get a sweet free GUI if you use [Rundeck](http://rundeck.org). Combined with
[rundeck-fabric](https://github.com/balanced-cookbooks/rundeck-fabric)
