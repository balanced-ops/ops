from __future__ import unicode_literals
import logging
import os

from fabric.api import env

import es
import logs
import geoip

__version__ = '1.0.1'
env.use_ssh_config = True


def configure_logging():
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    )
    fablogger = logging.getLogger('fabfile')
    fablogger.setLevel(logging.INFO)


configure_logging()
