from __future__ import unicode_literals

import boto.ec2


def schedule(crontab):
    def annotate_function(func):
        setattr(func, 'schedule', crontab)
        return func
    return annotate_function


def glob_hostnames(expression, region='us-west-1'):
    matches = []
    conn = boto.ec2.connect_to_region(region)
    reservations = conn.get_all_instances()
    for reservation in reservations:
        for instance in reservation.instances:
            if expression in instance.tags.get('Name', ''):
                matches.append(instance)
    return [
        match.ip_address or match.private_ip_address
        for match in matches
    ]


def glob_hostname(expression):
    # this info comes from aws, so assuming that all hosts are alive
    return glob_hostnames(expression)[0]
