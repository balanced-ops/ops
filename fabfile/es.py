from __future__ import unicode_literals
from datetime import date, datetime, timedelta
import logging
import urlparse

from fabric.api import task, run, hosts
from fabric.context_managers import settings
from fabric_rundeck import cron

from .utils import glob_hostname


logger = logging.getLogger(__name__)


@cron('0 11 * * *')
@hosts(glob_hostname('balanced-es-1'))
@task
def optimize(base_url='http://localhost:9200'):
    """Optimize yesterday's ES dashboard logs and application/system log
    indices.

    """
    indexes = []
    # get today toordinal from remote
    today_toordinal = int(run(
        'python -c "from datetime import date; print date.today().toordinal()"'
    ))
    # default to optimize yesterday log
    yesterday = datetime.fromordinal(today_toordinal - 1)
    yesterday_str = yesterday.strftime('%Y%m%d')
    monthonly = yesterday.strftime('%Y%m')
    logger.info('Optimizing yesterday %s logs', yesterday)
    indexes.append('log-{}'.format(yesterday_str))
    indexes.append('dash-log-{}'.format(monthonly))
    for index in indexes:
        url = urlparse.urljoin(
            base_url,
            '/{}/_optimize?max_num_segments=2'.format(index),
        )
        logger.debug('POSTing to %s', url)
        run('curl -XPOST {}'.format(url))


@hosts(glob_hostname('balanced-es-1'))
@task
def optimize_all(base_url='http://localhost:9200'):
    """Optimize **ALL** of ES indexes, except for today.

    """
    indexes = []
    # get today toordinal from remote
    today_toordinal = int(run(
        'python -c "from datetime import date; print date.today().toordinal()"'
    ))
    # process all indexes except today
    today = date.fromordinal(today_toordinal)
    today_str = today.strftime('%Y%m%d')
    logger.info('Optimizing indexes of all but not today %s', today)
    indices = (
        run('ls /mnt/search/elasticsearch/elasticsearch/nodes/0/indices')
        .split()
    )
    indices = filter(lambda index: not index.endswith(today_str), indices)
    indexes.extend(indices)

    for index in indexes:
        url = urlparse.urljoin(
            base_url,
            '/{}/_optimize?max_num_segments=2'.format(index),
        )
        logger.debug('POSTing to %s', url)
        run('curl -XPOST {}'.format(url))


@cron('0 11 * * *')
@hosts([glob_hostname('bsrch-{}'.format(h)) for h in ('live', 'integration')])
@task
def optimize_resources(base_url='http://localhost:9200'):
    """Optimize ES resources index. This is used for searching.

    """
    indexes = []
    logger.info('Optimizing resources index')
    indexes.append('resources')
    for index in indexes:
        url = urlparse.urljoin(
            base_url,
            '/{}/_optimize?max_num_segments=2'.format(index),
        )
        logger.debug('POSTing to %s', url)
        run('curl -XPOST {}'.format(url))


@cron('0 11 * * *')
@hosts(glob_hostname('balanced-es-1'))
@task
def purge_outdated(max_age_days='45'):
    """Purge outdated logs"""
    # rundeck passes args as strings
    max_age_days = int(max_age_days)
    if max_age_days < 30:
        raise Exception("ERROR: Refusing to delete logs less than 30 days old")
    timestamp = datetime.utcnow() - timedelta(days=max_age_days)
    cutoff = "log-{}".format(timestamp.strftime('%Y%m%d'))

    idx_loc = '/mnt/search/elasticsearch/elasticsearch/nodes/0/indices'
    idx_set = run('ls -1 -d {}/log-* | xargs -n1 basename'.format(idx_loc))
    idx_set = [s.strip() for s in idx_set.split('\n')]
    targets = [t for t in idx_set if t < cutoff]
    # remove each old index from es
    with settings(warn_only=True):
        for idx in targets:
            uri = "http://localhost:9200/{}".format(idx)
            logger.info('Deleting %s index from es', idx)
            # logger.info('Executing: curl -XDELETE {}'.format(uri))
            run('curl -XDELETE {}'.format(uri))
