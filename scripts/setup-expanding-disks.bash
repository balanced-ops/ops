#!/bin/bash
set -x
set -e

function create_physical_volume() {
    FS_DEVICE=${1}

    if [[ ! $(pvdisplay --colon) == *${FS_DEVICE}* ]]
    then
            pvcreate ${FS_DEVICE}
    else
        echo "Skipping creating physical volume '${FS_DEVICE}', already exists"
    fi
}

function create_volume_group() {
    FS_DEVICE=${1}
    if [[ ! $(vgdisplay --colon) == *vg0* ]]
    then
            vgcreate vg0 ${FS_DEVICE}
    else
        echo "Skipping creating volume group 'vg0', already exists"
    fi
}

function create_logical_volume() {

    function lvcreate_if_doesnt_exist () {
        LOGICAL_VOLUME=${1}
        OPTS=${2}
        if [[ ! $(lvdisplay --colon) == *${LOGICAL_VOLUME}* ]]
        then
                lvcreate ${OPTS} -n ${LOGICAL_VOLUME} vg0
        else
            echo "Skipping creating logical volume '${LOGICAL_VOLUME}', already exists"
        fi
    }

    lvcreate_if_doesnt_exist data "-l 8%vg"
    lvcreate_if_doesnt_exist var "-l 46%vg"
    lvcreate_if_doesnt_exist opt "-l 46%vg"
}

function create_file_system() {
    for d in data var opt; do
        if [[ -z $(blkid -s TYPE -o value /dev/vg0/${d}) ]]
        then
            mkfs.xfs /dev/vg0/${d}
        fi

    done
}

function make_directories() {
    FS_ROOT=${1}
    mkdir -p ${FS_ROOT}
    mkdir -p /mnt/opt
    mkdir -p /mnt/var
}

function update_fstab() {
    FS_ROOT=${1}
    FS_DEVICE=${2}

    # get all the disks associated
    DEVICES=$(fdisk -l 2>/dev/null  |
                      grep -v swap  |
                      grep 'Disk /' |
                      sed 's@.*\(/dev.*\):.*@\1@g')
    NEW_FSTAB=$(cat <<EOF | python -
devices = [d.strip() for d in '''${DEVICES}'''.split()]
with open('/etc/fstab', 'rb') as fstab:
    for line in fstab:
        if line.startswith('LABEL'):
            print line
            continue
        if line.startswith('${FS_DEVICE}'):
            continue
        if line.split(' ', 1)[0] in devices:
            print line
EOF
)
    mv /etc/fstab /etc/fstab.old
    echo "${NEW_FSTAB}" >> /etc/fstab
    # xfs defaults,auto,noatime,noexec 0 0
    FSTAB_SETTINGS="ext3 noatime,nodiratime 0 0"
    if [[ -z $(grep '/dev/mapper/vg0-data' /etc/fstab) ]]; then
        echo "/dev/mapper/vg0-data ${FS_ROOT} ${FSTAB_SETTINGS}" | tee -a /etc/fstab
    fi
    for d in opt var; do
        if [[ -z $(grep "/dev/mapper/vg0-${d}" /etc/fstab) ]]; then
            echo "/dev/mapper/vg0-${d} /${d} ${FSTAB_SETTINGS}" | tee -a /etc/fstab
        fi
    done
    cat /etc/fstab
}

function mount_filesystems() {
    FS_ROOT=${1}
    if [[ -z $(mount | grep /dev/mapper/vg0-data) ]]; then
        mount -v /dev/mapper/vg0-data ${FS_ROOT}
    fi
    for d in opt var; do
        if [[ -z $(mount | grep /dev/mapper/vg0-${d}) ]]; then
            mount -v /dev/mapper/vg0-${d} /mnt/${d}
        fi
    done
}

function remount_all() {
    set +e
    (mount -a || true)
    for d in var opt; do
      echo "For these processes, holding something open in ${d}:"
      echo `lsof +D /${d}`
      kill -SIGHUP `lsof -t +D /${d}`
    done
    # clean out old data in /var and /opt
    mkdir -p /mnt/tmproot
    mount -v / /mnt/tmproot
    rsync -avHP --delete /mnt/tmproot/var/ /var/
    rsync -avHP --delete /mnt/tmproot/opt/ /opt/
    umount /mnt/tmproot
    set -e
}

function initialize_file_system() {
    FS_ROOT=${1}
    FS_DEVICE=${2}
    make_directories ${FS_ROOT}
    mount_filesystems ${FS_ROOT}
    cp -Rp /var/* /mnt/var/
    cp -Rp /opt/* /mnt/opt/
    update_fstab ${FS_ROOT} ${FS_DEVICE}
    remount_all
    chmod 755 ${FS_ROOT}
}

DATA_DEVICE=/dev/xvdb
DATA_ROOT=/mnt/data

apt-get install -y lvm2 # xfsdump xfsprogs
create_physical_volume ${DATA_DEVICE}
create_volume_group ${DATA_DEVICE}
create_logical_volume ${DATA_DEVICE}

create_file_system
initialize_file_system ${DATA_ROOT} ${DATA_DEVICE}