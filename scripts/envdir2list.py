#!/usr/bin/bin python
"""
http://smarden.org/runit/chpst.8.html
runit/envdir compatibility:

- If dir contains a file named k whose first line is v, chpst removes
  the environment variable k if it exists, and then adds the
  environment variable k with the value v.

- The name k must not contain =.

- Spaces and tabs at the end of v are removed, and nulls in v are changed to newlines.

- If the file k is empty (0 bytes long), chpst removes the environment
  variable k if it exists, without adding a new variable.

https://docs.docker.com/reference/commandline/cli/#run
docker env-file compatibility:
- expects each line to be in the VAR=VAL format
- comment lines need only be prefixed with #
- If the file k is empty (0 bytes long), it acts as a "passthrough"

# phusion:baseimage-docker image
https://github.com/phusion/baseimage-docker#centrally-defining-your-own-environment-variables
- runit compatible

Usage:

Assuming your `pwd`/env directory looks like this:
`pwd`/env
|-- BAR
|-- BAZ
|__ FOO

$ ./envdir2list.py `pwd`/env
BAR=bar
BAZ=baz
FOO=foo

So, you can:

$ ./envdir2list.py `pwd`/env > docker.envlist

Then, docker run --env-file=docker.envlist ..

"""
from __future__ import unicode_literals
# TODO: maybe use optparse for python2.6 compat?
import argparse
import os


def execute(args):
    # or..
    # for item in `ls -1 env`; do echo $item=$(cat env/$item); done
    environment_vars = os.listdir(args.env_dir)
    env_list_contents = []
    for key in environment_vars:
        with open(os.path.join(args.env_dir, key), 'r') as key_file:
            variable = key_file.read()
            # TODO: runit/docker compatibility layer?
            variable = variable.strip()
        env_list_contents.append('{}={}'.format(key, variable))
    print '\n'.join(env_list_contents)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        dest='env_dir',
        help='Directory where environment variables are envdir compatabile'
    )
    args = parser.parse_args()
    execute(args)


if __name__ == '__main__':
    main()
